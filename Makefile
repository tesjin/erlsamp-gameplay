all: compile

compile: get-deps
	@rebar compile

get-deps:
	@rebar get-deps

clean:
	@rebar clean
	rm -rf log/
	rm -rf logs/
	rm -f erl_crash.dump

config:
	cp rel/files/sys.config.example sys.config

start:
	ERL_LIBS=deps erl -config sys.config -pa ebin -sname gamemode@localhost -setcookie erlsamp -s esg

build: clean compile tests
	mkdir -p builds
	rm -f builds/esg_b`date +%Y%m%d`.tar.gz
	rm -rf rel/gamemode/
	@rebar generate
	cd rel/ && tar -zcf ../builds/esg_b`date +%Y%m%d`.tar.gz gamemode/* && cd ..

.PHONY: compile
