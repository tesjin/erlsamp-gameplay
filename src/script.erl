%%% ==================================================================
%%% This file is part of the ErlSAMP Project.
%%% See AUTHORS file for Copyright information
%%% ==================================================================
-module(script).
-compile(export_all).

-define(SERVER, script_srv).

set_game_mode_text(Text) ->
  gen_server:cast({global, ?SERVER}, {set_game_mode_text, Text}).

set_interior(PlayerId, Interior) ->
  gen_server:cast({global, ?SERVER}, {set_interior, PlayerId, Interior}).

set_player_pos(PlayerId, X, Y, Z) ->
  gen_server:cast({global, ?SERVER}, {set_player_pos, PlayerId, X, Y, Z}).

set_player_rotation(PlayerId, Rotation) ->
  gen_server:cast({global, ?SERVER}, {set_player_rotation, PlayerId, Rotation}).

set_camera_pos(PlayerId, X, Y, Z) ->
  gen_server:cast({global, ?SERVER}, {set_camera_pos, PlayerId, X, Y, Z}).

set_camera_look_at(PlayerId, X, Y, Z) ->
  gen_server:cast({global, ?SERVER}, {set_camera_look_at, PlayerId, X, Y, Z, 0}).
set_camera_look_at(PlayerId, X, Y, Z, Cut) ->
  gen_server:cast({global, ?SERVER}, {set_camera_look_at, PlayerId, X, Y, Z, Cut}).

send_client_message(PlayerId, Color, Text) ->
  gen_server:cast({global, ?SERVER}, {send_client_message, PlayerId, Color, Text}).

send_client_message_to_all(Color, Text) ->
  gen_server:cast({global, ?SERVER}, {send_client_message_to_all, Color, Text}).

show_player_dialog(PlayerId, DialogId, Style, Caption, Info, LeftBtn, RightBtn) ->
  gen_server:cast({global, ?SERVER}, {show_player_dialog, PlayerId, DialogId, Style, Caption, Info, LeftBtn, RightBtn}).

get_player_name(PlayerId) ->
  gen_server:cast({global, ?SERVER}, {get_player_name, PlayerId}).

clear_chat(PlayerId, Num) ->
  gen_server:cast({global, ?SERVER}, {clear_chat, PlayerId, Num}).

kick(PlayerId) ->
  gen_server:cast({global, ?SERVER}, {kick, PlayerId}).
