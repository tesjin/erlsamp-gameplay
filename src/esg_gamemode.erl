%%% ==================================================================
%%% This file is part of the ErlSAMP Project.
%%% See AUTHORS file for Copyright information
%%% ==================================================================
-module(esg_gamemode).
-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, gamemode).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({global, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} | {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, #state{}}.

-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()}, State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(init, _From, State) ->
  script:set_game_mode_text("ErlSAMP Basic Gameplay"),
  {reply, ok, State};
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast({on_player_join, PlayerId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:JOIN] ~ts has joined the game.", [PlayerId, PlayerName]),
  {noreply, State};

handle_cast({on_player_disconnect, PlayerId, Reason}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:LEAVE] ~ts has left the game (~B).", [PlayerId, PlayerName, Reason]),
  {noreply, State};

handle_cast({on_player_request_class, PlayerId, ClassId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:REQUESTCLASS] ~ts request class: ~B.", [PlayerId, PlayerName, ClassId]),
  {noreply, State};

handle_cast({on_player_spawn, PlayerId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:SPAWN] ~ts spawned.", [PlayerId, PlayerName]),
  {noreply, State};

handle_cast({on_player_death, PlayerId, KillerId, ReasonId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  KillerName = esg_players:get_player_name(KillerId),
  if
    KillerId =:= 16#FFFF ->
      lager:info("[~B:DEATH] ~ts -> (~B).", [PlayerId, PlayerName, ReasonId]);
    true ->
      lager:info("[~B:DEATH] ~ts -> ~ts (~B).", [PlayerId, PlayerName, KillerName, ReasonId])
  end,
  {noreply, State};

handle_cast({on_player_damage_vehicle, PlayerId, VehicleId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:VDAMAGE] ~ts <- ~B", [PlayerId, PlayerName, VehicleId]),
  {noreply, State};

handle_cast({on_player_wants_enter_vehicle, PlayerId, VehicleId, Passenger}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:WVENTER] ~ts <- ~B, passenger: ~B", [PlayerId, PlayerName, VehicleId, Passenger]),
  {noreply, State};

handle_cast({on_player_leave_vehicle, PlayerId, VehicleId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:VLEAVE] ~ts <- ~B", [PlayerId, PlayerName, VehicleId]),
  {noreply, State};

handle_cast({on_player_message, PlayerId, Message}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:CHAT] ~ts: ~ts", [PlayerId, PlayerName, Message]),
  {noreply, State};

handle_cast({on_player_command, PlayerId, Command}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:CMD] ~ts: ~ts", [PlayerId, PlayerName, Command]),
  {noreply, State};

handle_cast({on_player_weapon_shot, PlayerId, WeaponId, HitType, HitId, X, Y, Z}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:WEAPONSHOT] ~ts: ~B ~B ~B ~f ~f ~f", [PlayerId, PlayerName, WeaponId, HitType, HitId, X, Y, Z]),
  {noreply, State};

handle_cast({on_player_enter_checkpoint, PlayerId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:ECP] ~ts", [PlayerId, PlayerName]),
  {noreply, State};

handle_cast({on_player_leave_checkpoint, PlayerId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:LCP] ~ts", [PlayerId, PlayerName]),
  {noreply, State};

handle_cast({on_player_click_map, PlayerId, X, Y, Z}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:MAP] ~ts (~f ~f ~f)", [PlayerId, PlayerName, X, Y, Z]),
  {noreply, State};

handle_cast({on_dialog_response, PlayerId, DialogId, Response, ListItem, Text}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:DIALOGRESPONSE] ~ts (~B ~B ~B ~ts)", [PlayerId, PlayerName, DialogId, Response, ListItem, Text]),
  {noreply, State};

handle_cast({on_player_interior_change, PlayerId, InteriorId}, State) ->
  PlayerName = esg_players:get_player_name(PlayerId),
  lager:info("[~B:INTERIOR_CHANGE] ~ts (~B)", [PlayerId, PlayerName, InteriorId]),
  {noreply, State};

handle_cast(_Request, State) ->
  {noreply, State}.

-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()), State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{}, Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
