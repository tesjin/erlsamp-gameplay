%%% ==================================================================
%%% This file is part of the ErlSAMP Project.
%%% See AUTHORS file for Copyright information
%%% ==================================================================
-module(esg_app).
-behaviour(application).

%% Application callbacks
-export([start/0,
  start/2,
  stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->
  application:start(esg).

start(_StartType, _StartArgs) ->
  lager:start(),
  esg_sup:start_link().

stop(_State) ->
  ok.
